//
// Created on 2023/12/21.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#include <sys/stat.h>
#ifndef speaker_cleaner_pcm_generator_H
#define speaker_cleaner_pcm_generator_H

int generatePCM(int frequency);

#endif //speaker-cleaner_pcm_generator_H
