import resourceManager from '@ohos.resourceManager';
export const createWav: (frequency: number) => number;
export const getRawFileDescriptor: (resmgr: resourceManager.ResourceManager, path: string) => resourceManager.RawFileDescriptor;