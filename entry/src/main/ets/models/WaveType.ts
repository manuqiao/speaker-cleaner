export enum WaveType {
  SINE = 0,
  SQUARE,
  TRIANGLE,
  SAWTOOTH
}