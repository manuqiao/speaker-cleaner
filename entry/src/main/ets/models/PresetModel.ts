import { WaveType } from './WaveType'

export interface PresetModel {
  waveType: WaveType,
  frequency: number
}